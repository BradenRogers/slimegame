﻿using UnityEngine;

public class TextureVariationRandomizer : MonoBehaviour
{
    [SerializeField]
    private Material matVariation1;
    [SerializeField]
    private Material matVariation2;
    [SerializeField]
    private Material matVariation3;

    private void Start()
    {
        Renderer renderer = GetComponent<Renderer>();

        int randomInt = Random.Range(0, 3);

        if(randomInt == 0)
        {
            renderer.material = matVariation1;
        }
        else if(randomInt == 1)
        { 
			renderer.material = matVariation2;
		}
        else if(randomInt == 2)
        { 
			renderer.material = matVariation3;
		}

    }

}
