﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    //  what the camera will follow
    [SerializeField] private Transform _Target;

    //  tuning values
    [Header("Camera Tuning")]
    [SerializeField] private float _LerpSpeed = 10f;
    [SerializeField] private float _FollowDistance = 30f;
    [SerializeField] private Vector3 _FollowDirection;


    // Update is called once per frame
    void FixedUpdate ()
    {
        Vector3 targetPosition = Vector3.zero;

        if (_Target != null)
        {
            targetPosition = _Target.position + _FollowDirection.normalized * _FollowDistance;
        }

        //  interpolate camera position to target postion
        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.fixedDeltaTime * _LerpSpeed);
	}
}
