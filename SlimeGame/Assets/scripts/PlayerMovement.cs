﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	[Header ("Script Customization")]
	//FEEDBACK INFO: control rotation default (true) feels better.
	[SerializeField] private bool controlsRotation;
	//Invert axis variable changes the direction of the player (implemented because of the current angle of the camera.)
	[SerializeField] private float speed; //Speed defined by units per second

	private float currentX;
	private float currentY;
	private float currentZ;
	private float xDelta;
	private float zDelta;
	
	private float previousX;
	private float previousZ;
	
	private float targetX;
	private float targetZ;
	
	private bool isMoving;

	void Start (){
		currentX = transform.position.x; 
		currentY = transform.position.y;
		currentZ = transform.position.z;
		previousX = currentX;
		previousZ = currentZ;
		targetX = Mathf.Round(currentX);
		targetZ = Mathf.Round(currentZ);
		isMoving = false;
	} 

	void Update () {
		if (controlsRotation){
			xDelta = speed * Input.GetAxisRaw ("Horizontal");
			zDelta = speed * Input.GetAxisRaw ("Vertical");
		}
		else {
			xDelta = speed * Input.GetAxisRaw ("Vertical");
			zDelta = speed * -Input.GetAxisRaw ("Horizontal");
		}
		
		if ((xDelta != 0) || (zDelta!= 0)){
			DetermineNextTarget();
			if (!isMoving)
				isMoving = true;
		}
		
		//Ask the difference between updating location info (not moving) in update or fixedupdate
		
	}
	
	void FixedUpdate () {			
		if (isMoving){
			if (targetX > currentX)
				currentX += speed * Time.fixedDeltaTime;
			else
				if (targetX < currentX)
					currentX -= speed * Time.fixedDeltaTime;
			if (targetZ > currentZ)
				currentZ += speed * Time.fixedDeltaTime;
			else
				if (targetZ < currentZ)
					currentZ -= speed * Time.fixedDeltaTime;
			transform.position = new Vector3 (currentX, currentY, currentZ);
		}

		if (((targetX-previousX)*(targetX-currentX)<0) || ((targetZ-previousZ)*(targetZ-currentZ)<0)){
			if (xDelta * zDelta == 0){
				transform.position= new Vector3 (Mathf.Round(targetX), currentY, Mathf.Round(targetZ));
				currentX = transform.position.x;
				currentZ = transform.position.z;
				isMoving = false;
			}
			else {
				DetermineNextTarget ();
			}
		}
		
		previousX = currentX;
		previousZ = currentZ;
	}
	
	private void DetermineNextTarget (){
		if (xDelta > 0)
			targetX = Mathf.Round(currentX) + 1;
		else
			if (xDelta < 0)
				targetX = Mathf.Round(currentX) - 1;
		if (zDelta > 0)
			targetZ = Mathf.Round(currentZ) + 1;
		else
			if (zDelta < 0)
				targetZ = Mathf.Round(currentZ) - 1;
	}
}
