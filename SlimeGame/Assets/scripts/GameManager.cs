﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject pauseScreen;
	[SerializeField] private GameObject loseScreen;
	[SerializeField] private GameObject winScreen;
    //private SlimeAI[] slimes;
    private int timer = 20;
    [SerializeField] Text slimeCounter;
    [SerializeField] Button contineButton;
	
	static private bool isGameLost = false;
	static private bool isGameWon = false;

    private void Awake()
    {
		isGameLost = false;
		isGameWon = false;
		Time.timeScale = 1;
		
		pauseScreen.SetActive (false);
		loseScreen.SetActive (false);
		winScreen.SetActive (false);
		
        if(contineButton != null)
        {
            contineButton.onClick.AddListener(Unpause);
        }
        pauseScreen.SetActive(false);
        //slimes = FindObjectsOfType<SlimeAI>();
    }
    private void Update()
    {
		//Update UI with the size of the static list in UpdatedSlimeAI.
        slimeCounter.text = UpdatedSlimeAI.allSlimes.Count.ToString(":00");

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(Time.timeScale == 1) 
            {
                //Pause
                Pause();
            }
            else
            {
                //Unpause
                Unpause();
            }
        }
		
		//Used to use static method to change game condition. Change and move to corresponding function if GameManager is made a singleton.
		if (isGameWon)
        {
			Debug.Log ("Game Won");
			winScreen.SetActive (true);
			Time.timeScale = 0;
		}
		if (isGameLost)
        {
			Debug.Log ("Game Lost");
			loseScreen.SetActive (true);
			Time.timeScale = 0;
		}
    }

    private void Pause()
    {
        AudioListener.volume = 0;
        pauseScreen.SetActive(true);
        Time.timeScale = 0;
    }
    private void Unpause()
    {
        AudioListener.volume = 1;
        pauseScreen.SetActive(false);
        Time.timeScale = 1;
    }
	
	static public void GameLost ()
    {
		isGameLost = true;
	}
	
	static public void GameWon ()
    {
		isGameWon = true;
	}

}
