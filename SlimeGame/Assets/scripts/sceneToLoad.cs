﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class sceneToLoad : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private string scene;

    private void Start()
    {
        if(button != null)
        {
            button.onClick.AddListener(loadScene);
        }
    }

    private void loadScene()
    {
        SceneManager.LoadScene(scene);
    }
}
