﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Villager : NavAI
{
    private UpdatedSlimeAI[] allSlimes; // update when i make a new slime controller script and move it to a central script
    private Coroutine currentState = null;
    private WaitPoint targetWaitPoint = null;
    private WaitPoint[] usableWaitPoints = null;

    [SerializeField] private float moveSpeed = 3.5f;
    [SerializeField] private float runSpeed = 8f;
    [SerializeField] private int spawnPointDistance = 10;
    [SerializeField] private GameObject skeleton;
    private Vector3 spawnPoint;

    [Header("Audio")]
    //[SerializeField] private AudioSourceController deathSound;
    [SerializeField] private AudioSourceController moveSound;
    [SerializeField] private AudioSourceController idleSound;
    [SerializeField] private AudioSourceController panicSound;

    private bool isChased;


    protected override void UnitAwake()
    {
        //agent = GetComponent<NavMeshAgent>();
        allNPC.Add(this);
        agent.speed = moveSpeed;
        spawnPoint = transform.position;
    }

    private void Start()
    {
        hp = 1;
        NPCType = 1; // villager npc id
        isChased = false;
        SetState(idleState());
        allSlimes = FindObjectsOfType<UpdatedSlimeAI>();
    }
    private void Update()
    {
        anim.SetFloat("_Speed", transform.TransformVector(agent.velocity).magnitude);
        if(checkForSlime())
        {
            isChased = true;
            SetState(chasedState());
        }
        else
        {
            isChased = false;
        }
        if(hp <= 0)
        {
            Death();
        }
    }

    private void Death()
    {
        hasBeenKilled = true;
        agent.isStopped = true;
        GetComponent<Collider>().enabled = false;
        Instantiate(skeleton, transform.position, Quaternion.identity);
        //deathSound.Play();
        anim.SetTrigger("_Death"); //is triggering death state in animator BUT since it gets destroyed right away, it has not time to show animation.
        Destroy(this.gameObject);
    }

    #region stateMachine

    private void SetState( IEnumerator newState )
    {
        if(currentState != null)
        {
            StopCoroutine(currentState);
        }
        currentState = StartCoroutine(newState);
    }

    private IEnumerator idleState()
    {
        //idleSound.source.Play();
        moveSound.source.Stop();
        panicSound.source.Stop();
        anim.speed = 1;
        agent.acceleration = 8;
        agent.speed = moveSpeed;
        while(targetWaitPoint == null)
        {
            FindWaitPoint();
            yield return null;
        }
        SetState(movingState());
    }
    private IEnumerator movingState()
    {
        moveSound.Play();
        anim.speed = 1;
        agent.acceleration = 8;
        agent.speed = moveSpeed;
        agent.SetDestination(targetWaitPoint.transform.position);
        while(agent.remainingDistance > agent.stoppingDistance)
        {
            yield return null;

        }
        SetState(waitingState());
    }
    private IEnumerator waitingState()
    {
        idleSound.Play();
        while(targetWaitPoint != null && targetWaitPoint.waitValue < 1f)
        {
            yield return null;
        }
        targetWaitPoint = null;
        SetState(idleState());
    }
    private IEnumerator chasedState()
    {
        if(!panicSound.source.isPlaying)
        {
            panicSound.Play();
        }
        agent.acceleration = 15;
        agent.speed = runSpeed;
        anim.speed = 1;
        while(agent.remainingDistance > agent.stoppingDistance)
        {
            yield return null;
        }
        agent.speed = 0;
        SetState(idleState());
    }

    private void FindWaitPoint()
    {
        var waitPoint = WaitPoint.WaitPointList.GetRandomWaitPoint();

        if(waitPoint == null)
        {
            return;
        }
        if(waitPoint.currentNPC == NPCType && waitPoint.waitValue < 1f && (Vector3.Distance(waitPoint.transform.position, spawnPoint) < spawnPointDistance || isChased))
        {
            targetWaitPoint = waitPoint;
        }
        else
        {
            targetWaitPoint = null;
        }
    }

    private bool checkForSlime()
    {
        foreach(UpdatedSlimeAI currentSlime in UpdatedSlimeAI.allSlimes)
        {
            if(Vector3.Distance(currentSlime.gameObject.transform.position, transform.position) < detectionRange)
            {
                targetWaitPoint = null;
                return true;
            }
        }
        return false;
    }

    #endregion

    private void OnDestroy()
    {
        //Debug.Log ("Villager #" + allNPC.IndexOf(this) + " is destroyed.");
        allNPC.Remove(this); // Moved the removal of npcs from the list so they are removed the exact same frame that the npc is killed.
    }
}
