﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killAfterAudio : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField] private AudioSourceController audioController;

    private void Awake()
    {
        audioController.Play();
        audioSource = GetComponent<AudioSource>();
    }

    private void LateUpdate()
    {
        if(!audioSource.isPlaying)
        {
            Destroy(this.gameObject);
        }
    }
}
