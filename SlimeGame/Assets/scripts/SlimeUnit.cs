using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public abstract class SlimeUnit : MonoBehaviour
{

    [SerializeField] private int _MaxHP = 1;
    [SerializeField] public int _HP = 1;
    [SerializeField] private float _Speed = 5f;
    [SerializeField] protected float _RangeOfDetection = 30f;

    protected Rigidbody _RB;
    protected Animator _Anim;

    private void Awake()
    {
        _RB = GetComponent<Rigidbody>();
        _Anim = GetComponent<Animator>();

        UnitAwake();
    }

    protected abstract void UnitAwake();

}