﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSoundTrigger : MonoBehaviour {

	[SerializeField] private AudioClip movementAudio;
	private AudioSource audioEmitter;
	[SerializeField] private float effectSpacing = 0.5f;
	private float lastSpacing;
	[SerializeField] private float volumePercent = 1f;
	private float currentX, currentZ;
	private float previousX, previousZ;
	
	void Start () {
		audioEmitter = GetComponent<AudioSource>();
		lastSpacing = 0f;
		
		currentX = transform.position.x;
		currentZ = transform.position.z;
		previousX = currentX;
		previousZ = currentZ;
		
	}
	
	void Update () {
		currentX = transform.position.x;
		currentZ = transform.position.z;
		
		if ((currentX - previousX != 0) || (currentZ - previousZ != 0)){
			lastSpacing += Time.deltaTime;
			if (lastSpacing >= effectSpacing){
				lastSpacing -= effectSpacing;

				audioEmitter.PlayOneShot (movementAudio, volumePercent);
			}
		}
		else{
			lastSpacing = effectSpacing;
		}

		previousX = currentX;
		previousZ = currentZ;
	}
}
