﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerSpawner : MonoBehaviour
{
    [SerializeField] private GameObject villager;
    [SerializeField] private float timeToSpawn;
    [SerializeField] private int maxVillagers = 15;
    private int checkTimer = 60;
    private Villager[] allVillagers;
    private float spawnTimer;

    private void Start()
    {
        allVillagers = FindObjectsOfType<Villager>();
        spawnTimer = timeToSpawn;
    }

    private void Update()
    {
        checkTimer -= 1;
        if(checkTimer <= 0)
        {
            allVillagers = FindObjectsOfType<Villager>();
            checkTimer = 60;
        }

        if(allVillagers.Length < maxVillagers)
        {
            spawnTimer -= Time.deltaTime;
            if(spawnTimer <= 0)
            {
                Instantiate(villager, transform.position, transform.rotation);
                spawnTimer = timeToSpawn;
            }
        }
    }

}
