﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
public abstract class NavAI : MonoBehaviour
{
    //[SerializeField, Range(1,4), Tooltip("1: Villager, 2: Chicken, 3: Knight, 4: King.")]
    [HideInInspector] public int NPCType;
    //[SerializeField] protected float moveSpeed = 3.5f; //set in the inspector
    //[SerializeField] private Vector3 newPos;
    [HideInInspector] public Animator anim;
    protected Rigidbody rB;
    public int hp;
    public bool hasBeenKilled = false;
	//Must refactor further.
	[SerializeField] protected float detectionRange = 25f;
	[SerializeField] protected float attackSpeed = 1f;
	protected static List<NavAI> allNPC = new List<NavAI>();
	
    [HideInInspector]
	public NavMeshAgent agent;

    private void Awake()
    {
        rB = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
		agent = GetComponent<NavMeshAgent>();
        UnitAwake();
    }
    protected abstract void UnitAwake();
}
