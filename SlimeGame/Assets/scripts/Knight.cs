﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Knight : NavAI
{
	[Header("AI")]
	private Coroutine currentState = null;
	private WaitPoint targetWaitPoint = null;
	//private SlimeAI[] allSlimes; // change type to kevins when done
	[HideInInspector] public UpdatedSlimeAI targetSlime;
	[SerializeField] private float distanceToCheckForSlime = 4;
	[SerializeField] private float moveSpeed = 3.5f;
	[SerializeField] private float runSpeed = 8f;
    [SerializeField] private GameObject deadKnight;
	[Header("Battle")]
	[SerializeField, Tooltip("Time to when he attacks")]
	private float attackTime = 10;
	private float attackTimer;
	private bool isDead = false;
	private bool isInBattle = false;

    [Header("Audio")]
    [SerializeField] private AudioSourceController attackSound;
    //[SerializeField] private AudioSourceController deathSound;
    [SerializeField] private AudioSourceController detectSound;
    [SerializeField] private AudioSourceController idleSound;

	protected override void UnitAwake()
	{
		//agent = GetComponent<NavMeshAgent>();
		allNPC.Add (this);
		agent.speed = moveSpeed;
	}
	private void Start()
	{
		hp = 4;
		NPCType = 3; // knight npc id
		SetState(idleState());
		//allSlimes = FindObjectsOfType<SlimeAI>();
		attackTimer = attackTime;
	}

	private void Update()
	{
		anim.SetFloat("_Speed", transform.TransformVector(agent.velocity).magnitude);
		//Debug.Log("Knight HP: " + hp);
		if(hp <= 0)
		{
			Death();
		}
		if(isInBattle)
		{	
			attackTimer -= Time.deltaTime;
			if(attackTimer <= 0)
			{
				//targetSlime.anim.SetTrigger("Hit");
				targetSlime.hp -= 1;
				attackTimer = attackTime;
                if(!attackSound.source.isPlaying)
                {
                    attackSound.Play();
                }
			}
		}
		//Debug.Log(attackTimer);
		if(checkForSlime())
		{
			SetState(chaseState());
		}
	}

	private void Death()
	{
        hasBeenKilled = true;
        //deathSound.Play();
        Instantiate(deadKnight, transform.position, transform.rotation);
		StopAllCoroutines();
		isDead = true;
		anim.SetTrigger("_Death"); //is triggering death state in animator BUT since it gets destroyed right away, it has not time to show animation.
		Destroy(this.gameObject);
	}

	#region stateMachine

	private void SetState( IEnumerator newState )
	{
		if(!isDead)
		{
			if(currentState != null)
			{
				StopCoroutine(currentState);
			}
			currentState = StartCoroutine(newState);
		}
	}

	private IEnumerator idleState()
	{
		//allSlimes = FindObjectsOfType<SlimeAI>();
		//Debug.Log("idle");
		targetSlime = null;
		agent.isStopped = false;
		anim.speed = 1;
		agent.acceleration = 8;
		agent.speed = moveSpeed;
		while(targetWaitPoint == null)
		{
			FindWaitPoint();
			yield return null;
		}
		SetState(movingState());
	}

	private IEnumerator movingState()
	{
		//allSlimes = FindObjectsOfType<SlimeAI>();
		//Debug.Log("moving");
		targetSlime = null;
		anim.speed = 1;
		agent.acceleration = 8;
		agent.speed = moveSpeed;
		agent.SetDestination(targetWaitPoint.transform.position);
		if(gameObject != null)
		{
			while(agent.remainingDistance > agent.stoppingDistance)
			{
				yield return null;

			}
			SetState(waitingState());
		}
	}

	private IEnumerator waitingState()
	{
        idleSound.Play();
		//Debug.Log("waiting");
		while(targetWaitPoint != null && targetWaitPoint.waitValue < 1f)
		{
			yield return null;
		}
		targetWaitPoint = null;
		SetState(idleState());
	}

	private IEnumerator chaseState()
	{
        if(!detectSound.source.isPlaying)
        {
            detectSound.Play();
        }
		//Debug.Log("chase");
		if(!isDead)
		{
			agent.acceleration = 100;
			agent.speed = runSpeed;
			agent.SetDestination(targetSlime.transform.position/* + new Vector3(Random.Range(1,2), 0, Random.Range(1,2))*/);
			while(Vector3.Distance(transform.position, targetSlime.transform.position) > targetSlime.agent.stoppingDistance)           //(agent.remainingDistance > agent.stoppingDistance)
			{
				yield return null;
			}
			SetState(battleState());
		}
	}

	private IEnumerator battleState()
	{
		//Debug.Log("battle");
		if(!isDead)
		{
            targetSlime.GetComponent<UpdatedSlimeAI>().target = (this.gameObject);
			agent.isStopped = true;
            Vector3 direction = targetSlime.transform.position - transform.position;
            Quaternion toRotaion = Quaternion.FromToRotation(transform.forward, direction);
			while(targetSlime != null && targetSlime.hp > 0)
			{
                transform.rotation = Quaternion.Lerp(transform.rotation, toRotaion, 0.5f * Time.deltaTime);
                //transform.rotation = Quaternion.Lerp(transform.rotation, targetSlime.transform.rotation , Time.deltaTime * 0.5f);
				isInBattle = true;
				anim.SetBool("_Attack", true);
				yield return null;
			}
			anim.SetBool("_Attack", false);

			isInBattle = false;
			targetSlime = null;
			SetState(idleState());
		}
	}

	private void FindWaitPoint()
	{
		var waitPoint = WaitPoint.WaitPointList.GetRandomWaitPoint();

		if(waitPoint == null)
		{
			return;
		}
		if(waitPoint.currentNPC == NPCType && waitPoint.waitValue < 1f)
		{
			targetWaitPoint = waitPoint;
		}
		else
		{
			targetWaitPoint = null;
		}
	}

	private bool checkForSlime()
	{
		foreach (UpdatedSlimeAI currentSlime in UpdatedSlimeAI.allSlimes)
        {
			if (Vector3.Distance (currentSlime.transform.position, transform.position) < detectionRange){
				targetSlime = currentSlime;
				return true;
			}
		}
		return false;
		/* if(allSlimes == null)
		{
			return false;
		}
		for(int i = 0; i < allSlimes.Length; i++)
		{
			if(allSlimes[i] != null)
			{
				if(Vector3.Distance(allSlimes[i].transform.position, transform.position) < detectionRange)
				{
					targetSlime = allSlimes[i];
					return true;
				}
			}
		}
		return false; */
	}

	#endregion stateMachine
	
	private void OnDestroy ()
	{
		allNPC.Remove(this); //Look at villager code for removal clarification.
	}
}
