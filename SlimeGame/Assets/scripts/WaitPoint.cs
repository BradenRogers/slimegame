﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitPoint : MonoBehaviour
{
    public static List<WaitPoint> WaitPointList = new List<WaitPoint>();

    public float waitValue
    {
        get;
        private set;
    }
    [SerializeField, Range(1,4), Tooltip("1: Villager, 2: Chicken, 3: Knight, 4: Archer.")] public int currentNPC = 1; // defult villager
 
    [SerializeField, Tooltip("Time in seconds")] private float waitTime = 5f;

    //private SkinnedMeshRenderer _FlagRender;

    private void OnEnable()
    {
        //_FlagRender = GetComponentInChildren<SkinnedMeshRenderer>();
        WaitPointList.Add(this);
    }

    private void OnDisable()
    {
        WaitPointList.Remove(this);
    }
    private void Update()
    {
        if(waitValue > 1.05f)
        {
            waitValue = 0;
        }
    }

    private void OnTriggerStay( Collider other )
    {
        NavAI u = other.GetComponent<NavAI>();
        if(u == null)
        {
            //waitValue = 0f;
            return;
        }
        //Debug.Log(waitValue);
        if(u.NPCType == currentNPC)
        {
             waitValue += Time.fixedDeltaTime / waitTime;
            if(waitValue > 1f)
            {
                //waitValue = 1f;
            }
        }
        else
        {
            waitValue = 0f;
            /*waitValue -= Time.fixedDeltaTime / waitTime;
            if(waitValue <= 0f)
            {
                waitValue = 0f;
                currentNPC = u.NPCType;
            }*/
        }
    }
}

public static class ListExtentions
{
    public static WaitPoint GetRandomWaitPoint( this List<WaitPoint> lst )
    {
        if(lst.Count > 0)
        {
            int rnd = Random.Range(0, lst.Count);
            return lst[rnd];
        }
        return null;
    }
}