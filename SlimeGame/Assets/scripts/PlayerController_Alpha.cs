using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
class PlayerController_Alpha : MonoBehaviour
{

    [SerializeField] private GameObject AOEGameObject;
    [SerializeField] private float playerSpeed = 5f;
    [SerializeField] private AudioSourceController bellAudio;
    [SerializeField] private GameObject bellParticlesWave;
	[SerializeField] private GameObject bellParticlesRelease;
    [SerializeField] private GameObject bellParticlesSprites;
    [SerializeField] private KeyCode slimeControlKey = KeyCode.Space;
    [SerializeField] private GameObject clickEffect;
    [SerializeField] private LayerMask groundLayer;
    private ParticleSystem ps;

    [SerializeField] private float decayTime = 5f;
    private float slimeControlKeyCooldown = 0.2f;
    private float decayDelta;

    [SerializeField] private bool controlsRotation = false;
    private NavMeshAgent agent;
    private Rigidbody myRigidbody;
    private Collider areaOfEffect;
    private float xInput, zInput;
    public static List<GameObject> followingSlimes;
    private Animator animator;

	private bool slimeControlAnimUse = false;
    private bool controlSlimes;

    public void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        ps = bellParticlesSprites.GetComponent<ParticleSystem>();
        // this doesnt do what you think it does
        //var main = ps.main;
        //main.duration = decayTime; //sets the spirtes duration to same as effect duration
        myRigidbody = GetComponent<Rigidbody>();
        areaOfEffect = AOEGameObject.GetComponent<Collider>();
        animator = GetComponentInChildren<Animator>();
        followingSlimes = new List<GameObject>();
    }

    public void Start()
    {
        areaOfEffect.enabled = false;
        controlSlimes = false;
        decayDelta = 0f;
    }

    public void Update()
    {
        xInput = Input.GetAxis("Horizontal");
        zInput = Input.GetAxis("Vertical");

        animator.SetFloat("_Speed", transform.TransformVector(agent.velocity).magnitude);

        if(Input.GetButtonDown("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if(Physics.Raycast(ray, out hitInfo, 100,groundLayer))
            {
                Instantiate(clickEffect, hitInfo.point, Quaternion.Euler(90,0,0));
                agent.SetDestination(hitInfo.point);
            }
        }
        
		
		if (Input.GetKeyUp(slimeControlKey) || Input.GetMouseButtonDown(1))
        {
			areaOfEffect.enabled = !controlSlimes;
			if (controlSlimes){
				bellParticlesSprites.SetActive (false);
				bellParticlesRelease.SetActive (true);
				ReleaseSlimes ();
			}
			else {
				if (!slimeControlAnimUse){
					animator.SetTrigger ("Command");
				}
			}
			controlSlimes = !controlSlimes;
		}

        if(Input.GetKey(KeyCode.L))
        {
            followingSlimes.Clear();
            foreach(GameObject currSlime in followingSlimes)
            {
                Debug.Log(currSlime);
            }
        }
    }

    public void FixedUpdate()
    {
		
        /*if(controlsRotation)
            myRigidbody.velocity = transform.TransformVector(new Vector3(xInput, 0f, zInput) * playerSpeed);
        else
            myRigidbody.velocity = transform.TransformVector(new Vector3(-zInput, 0f, xInput) * playerSpeed);*/
    }

    public void OnTriggerEnter( Collider other )
    {
        if(other.tag == "Slime")
        {
            if(!followingSlimes.Contains(other.gameObject))
            {
                //print("added slime");
                followingSlimes.Add(other.gameObject);
                other.GetComponent<UpdatedSlimeAI>().FollowPlayer(this.gameObject);
            }
        }
    }

    private void ReleaseSlimes()
    {
        //foreach(GameObject currentSlime in followingSlimes)
        //{
        //    currentSlime.GetComponent<UpdatedSlimeAI>().StopFollowPlayer(this.gameObject);
        //}

        for(int i = 0; i < UpdatedSlimeAI.allSlimes.Count; i++)
        {
            UpdatedSlimeAI.allSlimes[i].GetComponent<UpdatedSlimeAI>().StopFollowPlayer();
        }
        followingSlimes.Clear();
        Debug.Log(followingSlimes.Count);
    }

    public void PlayBellAudio()
    {
        bellAudio.Play();
        bellParticlesWave.SetActive(false);
        bellParticlesWave.SetActive(true);
        bellParticlesSprites.SetActive(true);
    }
	
	public void FinishedControlAnimation (){
		slimeControlAnimUse = false;
	}
}