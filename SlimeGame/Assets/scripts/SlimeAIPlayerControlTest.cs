using System.Collections;
using UnityEngine;
using UnityEngine.AI;


//Test version of SlimeAI. Tested code ported to main SlimeAI script.
[RequireComponent(typeof(NavMeshAgent))]
public class SlimeAIPlayerControlTest : SlimeUnit
{

    [SerializeField] private GameObject _Target;

    private NavAI[] enemies;
    private Coroutine _CurrentState = null;
    private NavMeshAgent _Agent;

    protected override void UnitAwake()
    {
        _Agent = GetComponent<NavMeshAgent>();
    }

    private void Awake()
    {
        _Agent = GetComponent<NavMeshAgent>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _RangeOfDetection);
    }

    private void Start()
    {
        SetState(State_Idle());
        enemies = FindObjectsOfType<NavAI>();
    }


    private void Update()
    {
        //_Anim.SetFloat("_Speed", transform.TransformVector(_Agent.velocity).magnitude);
        //State_MovingToTarget();
        LookForTarget();
    }

    #region StateMachine
    private void SetState(IEnumerator newState)
    {
        if (_CurrentState != null)
        {
            StopCoroutine(_CurrentState);
        }
        _CurrentState = StartCoroutine(newState);
    }

    private IEnumerator State_Idle()
    {
        while (_Target == null)
        {
            LookForTarget();
            yield return null;
        }

        SetState(State_MovingToTarget());
    }

    private IEnumerator State_MovingToTarget()
    {

        while (_Agent.remainingDistance > _Agent.stoppingDistance)
        {
            yield return null;
        }
        //_Target = null;
        SetState(State_Idle());
    }
	
	private IEnumerator State_FollowPlayer (){
		while (true){
			_Agent.SetDestination (_Target.transform.position);
			yield return null;
		}
		//yield return new WaitForSeconds (0.5f); //Setup to wait x time instead of updating target every frame to reflect "dumbness". Might be better to make Serialized private, but not sure how useful it is.
	}

    #endregion StateMachine


    private void LookForTarget()
    {

        for (int i = 0; i < enemies.Length; i++)
        {
            if (Vector3.Distance(enemies[i].transform.position, transform.position) < _RangeOfDetection)
            {
                _Agent.SetDestination(enemies[i].transform.position);
            }
        }


        // if(target.CurrentTeam != TeamNumber || outpost.CaptureValue < 1f)
        // {
        // 	_Target = target;
        // }
        // else
        // {
        // 	_Target = null;
        // }

    }
	
	
	public void FollowPlayer (GameObject other){
		if (other.tag == "Player"){
			_Target = other;
			SetState (State_FollowPlayer());
		}
	}
	
	public void StopFollowPlayer (GameObject other){
		if (other.tag == "Player"){
			_Target = null;
			SetState (State_Idle());
		}
	}

}
