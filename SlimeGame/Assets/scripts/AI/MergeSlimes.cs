﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class MergeSlimes : MonoBehaviour
{

	[SerializeField]
	private SlimeType _SlimeType = SlimeType.Small;

	[SerializeField]
	private GameObject _MergeIntoPrefab;

	[HideInInspector]
	public bool IsMerging;

	private void Awake()
	{
		IsMerging = false;
	}

	public SlimeType GetSlimeType()
	{
		return _SlimeType;
	}

	private void OnTriggerEnter(Collider other)
	{
		if(IsMerging) return; 

		var otherSlime = other.gameObject.GetComponent<MergeSlimes>();
		if(otherSlime != null && otherSlime.IsMerging == false && otherSlime.GetSlimeType() == _SlimeType)
		{
			otherSlime.IsMerging = true;
			IsMerging = true;
			Instantiate(_MergeIntoPrefab, transform.position, transform.rotation);
			Destroy(otherSlime.gameObject);
			Destroy(gameObject);
		}
	}

}

public enum SlimeType
{
	Small,
	Meduim,
	Big
}
