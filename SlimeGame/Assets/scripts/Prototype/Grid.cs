﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{

    //[SerializeField] private GameObject slimes;
    //[SerializeField] private GameObject structure;
    Vector3 TruePos;
    private float gridSize = 1f;
    Rigidbody rb;
    [SerializeField] private Vector3 box;

    private void Start()
    {
        //rb = slimes.GetComponent<Rigidbody>();
        //TruePos.y = 4;
    }

    private void LateUpdate()
    {
       //slimes = GameObject.FindGameObjectWithTag("Selected");
        /*TruePos.x = Mathf.Floor(slimes.transform.position.x / gridSize) * gridSize;
        //TruePos.y = Mathf.Floor(target.transform.position.y / gridSize) * gridSize;
        TruePos.y = 0.6f;
        TruePos.z = Mathf.Floor(slimes.transform.position.z / gridSize) * gridSize;

        //structure.transform.position = TruePos;*/

    }

    public Vector3 getNearestPointOnGrid( Vector3 position )
    {
        position -= transform.position;


        int xCount = Mathf.RoundToInt(position.x / gridSize);
        int yCount = Mathf.RoundToInt(position.y / gridSize);
        int zCount = Mathf.RoundToInt(position.z / gridSize);

        Vector3 result = new Vector3
            (
            (float)xCount * gridSize,
            (float)yCount * gridSize,
            (float)zCount * gridSize
            );
        result += transform.position;
        return result;
    }

    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        for(float x = 0; x < 40; x += gridSize)
        {
            for(float z = 0; z < 40; z += gridSize)
            {
                var point = getNearestPointOnGrid(new Vector3(x, 0f, z));
                Gizmos.DrawSphere(point, 0.1f);
            }
        }
    }*/

}
