﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerController : MonoBehaviour
{
    private Grid grid;
    [SerializeField] private LeadSlimeMovement[] slimes;
    private Vector3 direction;
    private Vector3 newPosition;
    private Vector3 test;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float moveDistance;
    private float step;
    private float x = 1;
    private int timer = 0;
    private bool isEaten;

    private void Start()
    {
        grid = FindObjectOfType<Grid>();
        slimes = FindObjectsOfType<LeadSlimeMovement>();
        direction = transform.position;
        //direction.y = 0.5f;
    }

    private void Update()
    {

        step = moveSpeed * Time.deltaTime;
        //direction.y = 0.5f;

        for(int i = 0; i < slimes.Length; i++)
        {
            if((slimes[i].transform.position - this.transform.position).sqrMagnitude < 3 * 3)
            {
                RunAway(slimes[i].gameObject);
            }
            test = grid.getNearestPointOnGrid(direction);
            test.y = 0.0f;
            newPosition = test;
            if(isEaten == false)
            {
                transform.position = Vector3.MoveTowards(transform.position, newPosition, step);
            }
            //direction.y = -0.5f;
        }
    }


    private void RunAway(GameObject closestSlime)
    {
        transform.LookAt(newPosition);
        if(closestSlime.transform.position.x > transform.position.x)
        {
            //Debug.Log("right");
            //right on screen
            direction.x = transform.position.x - moveDistance;
        }
        if(closestSlime.transform.position.x < transform.position.x)
        {
            //Debug.Log("left");
            //left on screen
            direction.x = transform.position.x + moveDistance;
        }
        if(closestSlime.transform.position.z > transform.position.z)
        {
            //Debug.Log("up");
            //up on screen
            direction.z = transform.position.z - moveDistance;
        }
        if(closestSlime.transform.position.z < transform.position.z)
        {
            //Debug.Log("down");
            //down on screen
            direction.z = transform.position.z + moveDistance;
        }
        
    }


    private void Eaten(GameObject attackingSlime)
    {
        isEaten = true;
        transform.localScale = new Vector3(x, x, x);
        x -= 0.05f;
        //GetComponent<BoxCollider>().isTrigger = true;
        
        transform.position = Vector3.Lerp(transform.position, attackingSlime.transform.position, 200);


        if(x <= 0.2f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter( Collision collision )
    {
        
    }
    private void OnCollisionStay( Collision collision )
    {
        Eaten(collision.gameObject);
    }

}
//direction = (closestSlime.transform.position - transform.position);
////if(Vector3.Angle(transform.position, closestSlime.transform.position) >= 20.0f)
//        //{
//        //    direction = (transform.position);
//        //    direction.x = transform.position.x + 5;
//        //    newPosition = grid.getNearestPointOnGrid(direction);
//        //    newPosition.y = 0.5f;
//        //}
//        Debug.Log(direction);
//        //transform.position = Vector3.MoveTowards(transform.position, -direction, step);
//        transform.position = transform.InverseTransformVector(-closestSlime.transform.position);