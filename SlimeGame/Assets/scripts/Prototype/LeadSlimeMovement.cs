﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeadSlimeMovement : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float moveSpeed = 3f;
    [SerializeField] private bool isMoving = false;
    [SerializeField] private Vector3 newPosition;
    private Rigidbody rb;
    private BoxCollider boxCollider;
    [Header("Debug")]
    private Grid grid;
    [SerializeField] private float groundLevel = 0.6f;
    private GameObject[] leadSlimes;
    private Animator animCont;
    [SerializeField] private bool isEating;
    [SerializeField] private GameObject chickenParts;
    [SerializeField] private int chickenSpeedIncrease = 3;
    [SerializeField] public int hp = 1;


    protected void Start()
    {
        animCont = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        grid = FindObjectOfType<Grid>();
    }

    private void Update()
    {
        if(hp <= 0)
        {
            Destroy(this.gameObject);
        }

        transform.LookAt(newPosition);

        animCont.SetBool("isMoving", isMoving);
        animCont.SetBool("isEating", isEating);

        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if(Physics.Raycast(ray, out hitInfo))
            {
                if(transform.tag == "Selected" && hitInfo.transform.tag != "Slime")
                {
                    SetNewLocation(hitInfo.point);
                }
                else if(hitInfo.transform.tag == "Slime")
                {
                    leadSlimes = GameObject.FindGameObjectsWithTag("Selected");
                    for(int i = 0; i < leadSlimes.Length; i++)
                    {
                        leadSlimes[i].gameObject.tag = "Slime";
                    }
                    hitInfo.collider.tag = "Selected";
                }
            }
        }
        if(transform.position == newPosition)
        {
            isMoving = false;
        }
        if(isMoving)  //&& transform.tag == "Selected"
        {
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, newPosition, step);
        }
        else
        {
            //transform.position = child.transform.position; 
        }
        if(transform.tag == "Selected")
        {
            
        }

    }
    private void SetNewLocation( Vector3 nearPoint )
    {
        var finalPosition = grid.getNearestPointOnGrid(nearPoint);
        finalPosition.y = groundLevel;
        newPosition = finalPosition;
        isMoving = true;
    }

    private void OnCollisionEnter( Collision collision )
    {
        //Debug.Log("hit");
        if(collision.collider.tag == "Civ" || collision.collider.tag == "Chicken")
        {
            Eat();
            if(collision.collider.tag == "Chicken")
            {
                chickenParts.gameObject.SetActive(true);
                moveSpeed += chickenSpeedIncrease;
            }
        }
        else
        {
            isEating = false;
        }
    }

    private void Eat()
    {
        isEating = true;
    }

}
