﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour
{
    private Grid grid;
    [SerializeField] private Vector3 mouseLocation;

    private void Start()
    {
        grid = FindObjectOfType<Grid>();
    }

    private void Update()
    {
        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray, out hitInfo))
        {
            setNewLocation(hitInfo.point);
        }
        transform.position = mouseLocation;
    }

    private void setNewLocation( Vector3 newPosition )
    {
        mouseLocation = grid.getNearestPointOnGrid(newPosition);
        mouseLocation.y = 0.02f;
    }

}
