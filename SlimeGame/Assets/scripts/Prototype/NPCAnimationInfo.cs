﻿using UnityEngine;
using UnityEngine.AI;

public class NPCAnimationInfo : MonoBehaviour 
{
	private Vector3 _Velocity;
	private NavMeshAgent _NMA;
	private Animator _Animator;

	private void Start()
	{
		_NMA = GetComponent<NavMeshAgent>();
		_Animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () 
	{
		_Velocity = _NMA.velocity;
		_Velocity = transform.TransformVector(_Velocity);
		var _Speed =  _Velocity.magnitude;
		_Animator.SetFloat("_Speed", _Speed);
		
	}

	
}
