﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonActivator : MonoBehaviour 
{
	private Rigidbody[] RB;
	[SerializeField] private float fallDelay;

	private void Start()
	{
		RB = GetComponentsInChildren<Rigidbody>();
		StartCoroutine(DelayedFall());
	}
	
	IEnumerator DelayedFall()
	{
		yield return new WaitForSeconds(fallDelay);
		foreach(Rigidbody Rigidbody in RB)
		{
			Rigidbody.useGravity = true;
			
		}
		yield return null;
	}

}
