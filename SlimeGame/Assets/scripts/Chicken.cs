﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Chicken : NavAI{

	private Coroutine currentState = null;
	private WaitPoint targetWaitPoint = null;
	private UpdatedSlimeAI[] allSlimes;
	[SerializeField] private int distanceToRunFromSlime = 4;
	private bool isScared = false;
	private UpdatedSlimeAI targetSlime = null;
	private ParticleSystem pS;
	private float rotation; // 400-600 
	private float runfoward; // 10-15
	[SerializeField] private GameObject deathParticles;
	private Vector3 spawnPoint;

    [Header("Audio")]
    [SerializeField] private AudioSourceController deathSound;
    [SerializeField] private AudioSourceController idleSound;
    [SerializeField] private AudioSourceController runSound;
    [SerializeField] private AudioSourceController suprisedSound;

	protected override void UnitAwake()
	{
		agent = GetComponent<NavMeshAgent>();
		pS = GetComponentInChildren<ParticleSystem>();
		NavAI.allNPC.Add(this);
		//agent.speed = moveSpeed;
	}

	private void Start()
	{
		hp = 1;
		NPCType = 2; // chicken npc id
		SetState(idleState());
		allSlimes = FindObjectsOfType<UpdatedSlimeAI>();
		pS.Pause();
		
		
	}

	private void Update()
	{
		anim.SetFloat("_Speed", transform.TransformVector(agent.velocity).magnitude);
		if(checkForSlime())
		{
			SetState(scaredState());
		}

		if(isScared)
		{
            runSound.Play();
			anim.SetFloat("_Speed", 1);
			//anim.speed = 2;
			transform.Rotate(Vector3.up * Time.deltaTime * rotation, Space.Self);
			transform.position += transform.forward * Time.deltaTime * runfoward;
		}

        if(hp <= 0)
        {
            Death();
        }
	}

    private void Death()
    {
        hasBeenKilled = true;
		Instantiate(deathParticles, transform.position, Quaternion.identity);
        deathSound.Play();
        Destroy(this.gameObject);
    }

	#region stateMachine

	private void SetState( IEnumerator newState )
	{
		if(currentState != null)
		{
			StopCoroutine(currentState);
		}
		currentState = StartCoroutine(newState);
	}

	private IEnumerator idleState()
	{
        idleSound.source.Stop();
        runSound.source.Stop();
        suprisedSound.source.Stop();
		while(targetWaitPoint == null)
		{
			FindWaitPoint();
			yield return null;
		}
		SetState(movingState());
	}
	private IEnumerator movingState()
	{
		agent.SetDestination(targetWaitPoint.transform.position);
		while(agent.remainingDistance > agent.stoppingDistance)
		{
			yield return null;
		}
		SetState(waitingState());
	}
	private IEnumerator waitingState()
	{
        idleSound.Play();
		while(targetWaitPoint.waitValue < 1f)
		{
			yield return null;
		}
		targetWaitPoint = null;
		SetState(idleState());

	}

	private IEnumerator scaredState()
	{
        if(!suprisedSound.source.isPlaying)
        {
            suprisedSound.Play();
        }
		rotation = Random.Range(100,200);
		runfoward = Random.Range(5,7);
		pS.Play();   
		isScared = true;
		while(true)
		{
            yield return new WaitForSecondsRealtime(3f);
		}

	}

	private bool checkForSlime()
	{
		if(allSlimes == null)
		{
			return false;
		}
		for(int i = 0; i < allSlimes.Length; i++)
		{
			if(allSlimes[i] != null)
			{
				if(Vector3.Distance(allSlimes[i].transform.position, transform.position) < detectionRange)
				{
					//targetWaitPoint = null;
					targetSlime = allSlimes[i];
					return true;
				}
			}
		}
		return false;
	}

	private void FindWaitPoint()
	{
		var waitPoint = WaitPoint.WaitPointList.GetRandomWaitPoint();

		if(waitPoint == null)
		{
			return;
		}
		if(waitPoint.currentNPC == NPCType && waitPoint.waitValue < 1f)
		{
			targetWaitPoint = waitPoint;
		}
		else
		{
			targetWaitPoint = null;
		}
	}

    /*private void OnTriggerEnter( Collider other )
    {
        if(other.gameObject.tag == "Slime")
        {
            Destroy(this.gameObject);
        }
    }*/

    #endregion

    private void OnDestroy ()
    {
		NavAI.allNPC.Remove (this); //Look at villager code for removal clarification.
	}
}
