﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SlimeMerging : LeadSlimeMovement {


	[SerializeField] private GameObject _Spawn;
	[SerializeField] private Vector3 _Scale;

	private void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Slime")
		{
			GameObject newSpawn = Instantiate(_Spawn, transform.position, transform.rotation) as GameObject;
			newSpawn.transform.localScale = new Vector3(_Scale.x, _Scale.y, _Scale.z);
			

			Destroy(collision.gameObject);
			Destroy(gameObject);
		}
	}
}
