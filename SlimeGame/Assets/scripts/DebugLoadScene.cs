﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugLoadScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Keypad1))
        {
			SceneManager.LoadScene("Farm2", LoadSceneMode.Single);
    	}
		else if (Input.GetKeyDown(KeyCode.Keypad2))
        {
			SceneManager.LoadScene("Village", LoadSceneMode.Single);
    	}
		else if (Input.GetKeyDown(KeyCode.Keypad3))
        {
			SceneManager.LoadScene("Castle", LoadSceneMode.Single);
    	}
	}
}
