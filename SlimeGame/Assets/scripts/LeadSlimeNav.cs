﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class LeadSlimeNav : MonoBehaviour
{
    private NavMeshAgent agent;
    [SerializeField] private Coroutine currentState = null;
    [SerializeField] private Vector3 newPosition;
    private GameObject target;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        SetState(idleState());
        agent.SetDestination(transform.position);
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                newPosition = hit.point;
            }
        }
    }

    #region StateMachine

    private void SetState( IEnumerator newState )
    {
        if(currentState != null)
        {
            StopCoroutine(currentState);
        }

        currentState = StartCoroutine(newState);
    }

    private IEnumerator idleState()
    {
        while(tag != "Selected")
        {
            agent.SetDestination(transform.position);
            yield return null;
        }
        //SetState(moveState());
    }

    private IEnumerator moveState()
    {
       // agent.SetDestination(newPosition);
        while(agent.remainingDistance > agent.stoppingDistance)
        {
            yield return null;
        }
        //SetState(idleState());
    }

    /*private IEnumerator chaseState()
    {
        agent.SetDestination(target.transform.position);

    }*/

    #endregion

    /*private void wander()
    {
        Vector3 randomPosition = 
        agent.SetDestination(randomPosition)
    }*/
}
