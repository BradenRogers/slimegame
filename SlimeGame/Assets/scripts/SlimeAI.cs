using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class SlimeAI : SlimeUnit
{
    [SerializeField]
    private float _AttackTime = 2;

    [SerializeField]
    private GameObject _Player;

    private static List<SlimeAI> _Slimes = new List<SlimeAI>();
    private SlimeUnit _SlimeTargets;
    private NavAI[] _Enemies;
    private NavAI _EnemyTargets;
    private Coroutine _CurrentState = null;
    private NavMeshAgent _Agent;
    private float _AttackTimer;
    private bool _Dead = false;
    private bool _Battle = false;

    //Eliminate / Fix when change of SlimeUnit to NavAI.
    protected override void UnitAwake()
    {
        _Agent = GetComponent<NavMeshAgent>();
    }

    private void Awake()
    {
        _Agent = GetComponent<NavMeshAgent>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _RangeOfDetection);
    }

    private void Start()
    {
        _HP = 1;
        SetState(State_Idle());
        _Enemies = FindObjectsOfType<NavAI>();
        //Debug.Log (_Enemies.Length);
        _Slimes.Add(this);
        _AttackTimer = _AttackTime;
    }


    private void Update()
    {
        //Debug.Log(_AttackTimer);
        //Debug.Log("Slime HP: " + _HP);
        //Debug.Log(_EnemyTargets.NPCType);
        //_Anim.SetFloat("_Speed", transform.TransformVector(_Agent.velocity).magnitude);
        //State_MovingToTarget();
        if (_HP <= 0)
        {
            //StopAllCoroutines();
            //_Dead = true;
            //gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
        LookForTarget();
    }

    #region StateMachine
    private void SetState(IEnumerator newState)
    {
        if (!_Dead)
        {
            if (_CurrentState != null)
            {
                StopCoroutine(_CurrentState);
            }
            _CurrentState = StartCoroutine(newState);
        }

    }

    //Plese fix.... This probably shouldn't be done like this.
    private IEnumerator State_SlimeMoveSound()
    {
        while (true)
        {
            //movementAudio.Play();
            yield return new WaitForSeconds(1f);

        }
    }

    private IEnumerator State_Idle()
    {
        _EnemyTargets = null;
        _SlimeTargets = null;
        _Agent.isStopped = false;
        while (_EnemyTargets == null)
        {
            LookForTarget();
            yield return null;
        }
        /*if(_EnemyTargets.NPCType == 3)
		{
			SetState(State_BattleState());
		}*/
        SetState(State_MovingToTarget());

    }

    private IEnumerator State_MovingToTarget()
    {
        _Agent.SetDestination(_EnemyTargets.transform.position);
        while (_Agent.remainingDistance > _Agent.stoppingDistance)
        {
            yield return null;
        }
        //_Target = null;
        SetState(State_Idle());

    }

    private IEnumerator State_FollowPlayer()
    {
        while (true)
        {
            _Agent.SetDestination(_Player.transform.position);
            yield return null;
        }

        //yield return new WaitForSeconds (0.5f); //Setup to wait x time instead of updating target every frame to reflect "dumbness". Might be better to make Serialized private, but not sure how useful it is.
    }

    private IEnumerator State_BattleState(NavAI target)
    {
        //Commented looking for enemies as slime will enter into the state with a target.
        /* _Agent.SetDestination(_EnemyTargets.transform.position);
		while (Vector3.Distance(transform.position, _EnemyTargets.transform.position) > 2)
		{
			yield return null;
		} */
        _Agent.isStopped = true;
        while (target.hp > 0)
        {
            _AttackTimer += Time.deltaTime;
            if (_AttackTimer > _AttackTime)
            {
                target.hp -= 1; //TODO: Modify to use an attack value variable to be set in Inspector.
                _AttackTimer -= _AttackTime;
            }

            yield return null;
        }
        //Commented section to be refactored by using the recieved target instead of the enemy list (TODO: clear enemy list).
        /* while (_EnemyTargets != null && _EnemyTargets.hp > 0)
		{
			_AttackTimer -= Time.deltaTime;
			if (_AttackTimer <= 0)
			{
				_EnemyTargets.hp -= 1;
				_AttackTimer = _AttackTime;
			}
			_Agent.isStopped = true;
			while(_EnemyTargets != null && _EnemyTargets.hp > 0)
			{
				_Battle = true;
				yield return null;
			}
			_Battle = false;
			_EnemyTargets = null;
		}
		_EnemyTargets = null; */
        SetState(State_Idle());

    }

    private IEnumerator State_Eating(GameObject target)
    {

        Debug.Log("Eating");

        //Add flair to eating.
        Destroy(target);

        //Put the yield return inside a while when polishing and adding delay to eating.
        yield return null;

        SetState(State_Idle());
    }

    #endregion StateMachine


    private void LookForTarget()
    {
        //	Search for player
        if (_Player == null) { return; }
        if (Vector3.Distance(_Player.transform.position, transform.position) < _RangeOfDetection)
        {
            Debug.Log("Following");
            //_Agent.SetDestination(_Enemies[i].transform.position);
            //_Agent.SetDestination(_Player.transform.position);
            FollowPlayer(_Player);
        }

        //	Search for enemies
        if (_Enemies == null)
        {
            return;
        }
        for (int i = 0; i < _Enemies.Length; i++)
        {
            if (_Enemies[i] != null)
            {
                if (Vector3.Distance(_Enemies[i].transform.position, transform.position) < _RangeOfDetection)
                {
                    //Debug.Log("Following");
                    //_Agent.SetDestination(_Enemies[i].transform.position);
                    _EnemyTargets = _Enemies[i];
                    if (_EnemyTargets.NPCType == 3)
                    {
                        //SetState(State_BattleState());
                    }
                }
            }
        }

        //  Find another slime to procreate with 
        if (_Slimes == null)
        {
            return;
        }

        for (int x = 0; x < _Slimes.Count; x++)
        {
            if (_Slimes[x] == gameObject)
            {
                continue;
            }
            if (_Slimes[x] != null)
            {
                if (Vector3.Distance(_Slimes[x].transform.position, transform.position) < _RangeOfDetection)
                {
                    //Debug.Log("Following");
                    //_Agent.SetDestination(_Enemies[i].transform.position);
                    _SlimeTargets = _Slimes[x];
                    _Agent.SetDestination(_SlimeTargets.transform.position);
                    //SetState(State_MovingToTarget());
                }
            }
        }
    }

    public void FollowPlayer(GameObject other)
    {
        if (other.tag == "Player")
        {
            _Player = other;
            SetState(State_FollowPlayer());
        }
    }

    public void StopFollowPlayer(GameObject other)
    {
        if (other.tag == "Player")
        {
            _Player = null;           //Throwing errors so i had to comment them out
            SetState(State_Idle());
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.collider.tag)
        {
            case "Player":
                SetState(State_FollowPlayer());
                break;
            case "Knight":
                SetState(State_BattleState(collision.gameObject.GetComponent<NavAI>()));
                break;
            case "Villager":
            case "Chicken":
            case "King":
                SetState(State_Eating(collision.gameObject));
                break;
        }

        //Changing to switch, leaving in case changes need to be recovered later.
        /* if (collision.collider.tag == "Villager")
		{
			Debug.Log("HIT");
			Destroy(gameObject);
		} */
    }

    private void OnDestroy()
    {
        //Code section copied from Kevin's section when checking health.
        StopAllCoroutines();
        _Dead = true;

        _Slimes.Remove(this);
        Debug.Log(_Slimes.Count);
        if (_Slimes.Count == 0)
            GameManager.GameLost();
    }

}
