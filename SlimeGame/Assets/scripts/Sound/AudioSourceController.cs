﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioSourceController : MonoBehaviour
{
    [SerializeField] private bool playOnAwake = false;
    public AudioClip[] sounds;
    public AudioSource source;
    public AudioMixerGroup mixerGroup;

    [Range(-80f, 0f)]
    public float volume;
    [Range(-12f, 0f)]
    public float randomVolMin;
    [Range(0f, 12f)]
    public float randomVolMax;

    [Range(-24f, 24f)]
    public float pitch;
    [Range(-24f, 0f)]
    public float randomPitchMin;
    [Range(-24f, 4f)]
    public float randomPitchMax;

    private void Awake()
    {
        if(playOnAwake)
        {
            Play();
        }
    }

    public void Play()
    {
        AudioClip clipToPlay = sounds[Random.Range(0, sounds.Length)];
        source.clip = clipToPlay;

        source.outputAudioMixerGroup = mixerGroup;
        

        float finalVolum = volume + Random.Range(randomVolMin, randomVolMax);
        finalVolum = AudioConvertionUtilities.dbtoLinear(finalVolum);   //  convert to linear for audio source
        source.volume = finalVolum;

        float finalPitch = pitch + Random.Range(randomPitchMin, randomPitchMax);
        finalPitch = AudioConvertionUtilities.stToPitch(finalPitch);   //  convert to linear for audio source
        source.pitch = finalPitch;

        source.Play();
    }
}
