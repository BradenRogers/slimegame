﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class KillZone : MonoBehaviour
{

    [SerializeField] private bool slimeKillZone;
    private void OnTriggerStay( Collider other )
    {
        if(!slimeKillZone)
        {
            if(other.tag == "Civ")
            {
                Debug.Log("Villager entered KillZone");
                Destroy(other.gameObject);
            }
        }
        else
        {
            if(other.tag == "Slime")
            {
                Debug.Log("Slime has Entered Killzone");
                Destroy(other.gameObject);
            }
        }
    }
}
