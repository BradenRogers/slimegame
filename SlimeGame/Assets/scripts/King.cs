using UnityEngine;
using System.Collections;
using UnityEngine.AI;


public class King : NavAI
{
    //private UpdatedSlimeAI[] allSlimes; // switch to use a global list or global array later
    private Coroutine currentState = null;
    private WaitPoint targetWaitPoint = null;
    private bool allAlone = false;

    [Header("Movement")]
    [SerializeField] private float moveSpeed = 3.5f;
    [SerializeField] private float runSpeed = 8f;

    [Space]
    [Header("Audio")]
    [SerializeField] private AudioSourceController alertSound;
    [SerializeField] private AudioSourceController idleSound;
    [SerializeField] private AudioSourceController deathSound;
    [SerializeField] private AudioSourceController commandSound;

    protected override void UnitAwake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = moveSpeed;
    }

    private void Start()
    {
        hp = 4;
        NPCType = 4;
        SetState(idleState());
       //allSlimes = FindObjectsOfType<UpdatedSlimeAI>();
		
		allNPC.Add (this);
    }

    private void Update()
    {
        anim.SetFloat("_Speed", transform.TransformVector(agent.velocity).magnitude);

        if(hp <= 0)
        {
            death();
        }
    }

    private void death()
    {
        deathSound.Play();
        Destroy(this.gameObject);
    }

    #region stateMachine

    private void SetState( IEnumerator newState )
    {
        if(currentState != null)
        {
            StopCoroutine(currentState);
        }
        currentState = StartCoroutine(newState);
    }

    private IEnumerator idleState()
    {
        idleSound.source.Stop();
        anim.speed = 1;
        agent.acceleration = 8;
        agent.speed = moveSpeed;
        while(targetWaitPoint == null)
        {
            FindWaitPoint();
            yield return null;  
        }
        SetState(movingState());
    }
    private IEnumerator movingState()
    {
        agent.SetDestination(targetWaitPoint.transform.position);
        while(agent.remainingDistance > agent.stoppingDistance)
        {
            yield return null;
        }
        SetState(waitingState());
    }
    private IEnumerator waitingState()
    {
        idleSound.Play();
        while(targetWaitPoint != null && targetWaitPoint.waitValue < 1f)
        {
            yield return null;
        }
        targetWaitPoint = null;
        SetState(idleState());
    }

    private void FindWaitPoint()
    {
        var waitPoint = WaitPoint.WaitPointList.GetRandomWaitPoint();
        if(waitPoint == null)
        {
            return;
        }
        if(waitPoint.currentNPC == NPCType && waitPoint.waitValue < 1f)
        {
            targetWaitPoint = waitPoint;
        }
        else
        {
            targetWaitPoint = null;
        }
    }

    #endregion stateMachine

    private void OnDestroy ()
    {
		GameManager.GameWon();
	}
	
}