using System.Collections.Generic;
using System.Collections;
//Using System to handle exceptions (dirty dirty fix). Erase when that section of the code is cleaned up.
using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class UpdatedSlimeAI : NavAI
{

    public static List<UpdatedSlimeAI> allSlimes = new List<UpdatedSlimeAI>();

    [Header("Slime Properties")]
    [SerializeField] private float deviationChance;
    [SerializeField] private GameObject duplicatedSlimePrefab;
    [SerializeField] private SlimeSize _SlimeSize = SlimeSize.Small;
    [SerializeField] private GameObject _MergeIntoPrefab;
    [SerializeField] private GameObject deathParticles;
    [SerializeField] private GameObject feather;
    [SerializeField] private GameObject wings;
    [SerializeField] private float eatingSpeed;
    [SerializeField] private int slimeType = 1; //Slimes fall under normal, chicken, and armored.

    [Header("Slime Mutations")]
    [SerializeField] private float normalSlimeSpeed = 1f;
    [SerializeField] private float chickenSlimeSpeed = 3f;
    [SerializeField] private float armorSlimeSpeed = 0.5f;
    [SerializeField] private int armorSlimeHp = 3;
    [SerializeField] private Material armorMaterial;
    [SerializeField] private Material chickenMaterial;
    [SerializeField] private Mesh armorMesh;
    [SerializeField] private Mesh chickenMesh;

    [Header("Audio")]
    [SerializeField] private AudioSourceController deathSound;
    [SerializeField] private AudioSourceController moveSound;
    [SerializeField] private AudioSourceController mergeSound;
    [SerializeField] private AudioSourceController eatingSound;

    [SerializeField]
    private bool IsMerging;
	
	private bool followingPlayer;

    private float waitTimer;
    private bool battle = false;
    private Coroutine currentState = null;
    [HideInInspector] public GameObject target;

    //Only to keep track using debug. Clear for final.
    [SerializeField] private string currentCoroutine;
    /// //////////////////////////////////////////////
    // all this stuff will have to be redone for final this is just for the beta

    /// //////////////////////////////////////////////


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }

    protected override void UnitAwake()
    {
        allSlimes.Add(this);
        IsMerging = false;
		followingPlayer = false;
    }

    private void Start()
    {
        ApplyType();
        SetState(State_Idle());


        if(_SlimeSize == SlimeSize.Small)
        {
            agent.stoppingDistance = 1;
        }
        else if(_SlimeSize == SlimeSize.Meduim)
        {
            agent.stoppingDistance = 2;
        }
        else if(_SlimeSize == SlimeSize.Big)
        {
            agent.stoppingDistance = 3;
        }
    }

    private void Update()
    {
        //Debug Command to keep track of the slime state by number.
        if (Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log("Slime #" + allSlimes.IndexOf(this) + ": " + currentCoroutine);
        }
        if (battle)
        {
            //transform.LookAt(target.transform);
            waitTimer -= Time.deltaTime;
            if (waitTimer <= 0 && target != null)
            {
                if(target != null && Vector3.Distance(target.transform.position, transform.position)< agent.stoppingDistance + 1) // quick fix to stop them from killing npc from a distance
                {
                    target.GetComponent<NavAI>().hp -= 1;
                }
                waitTimer = attackSpeed;
            }
        }

        if(hp <= 0)
        {
            Death();
        }
    }

    private void Death()
    {
        GameObject DP = Instantiate(deathParticles, transform.position, Quaternion.identity);
        DP.transform.localScale = transform.localScale;
        deathSound.Play();
        Destroy(this.gameObject);
    }

    private void SetState(IEnumerator newState)
    {
        if (currentState != null)
            StopCoroutine(currentState);
        currentState = StartCoroutine(newState);
    }

    private IEnumerator State_Idle()
    {
        moveSound.source.Stop();
        currentCoroutine = "Idle";
        agent.isStopped = false; // Sets the target in motion. Check if needed.
        while (!target)
        { //You'll never convince me to use == null. Not in the near future anyway.
            LookForTarget();
            yield return null;
        }

        SetState(State_MovingToTarget());
    }

    private IEnumerator State_MovingToTarget()
    {

        moveSound.Play();
        currentCoroutine = "MovingToTarget";
        do
        {
            //Very dirty fix to bug where target would point to null when another slime ate the target.
            //Find a better way to do.
            try
            {
                agent.SetDestination(target.transform.position);
            }
            catch (Exception e)
            {
                if (e is NullReferenceException || e is MissingReferenceException)
                {
                    target = null;
                    SetState(State_Idle());
                }
            }
            if (UnityEngine.Random.value < deviationChance)
                LookForTarget();
            yield return null;
            //Debug.Log (agent.remainingDistance - agent.stoppingDistance);
        } while (agent.remainingDistance > agent.stoppingDistance);

        if (target != null && (target.tag == "Knight" || target != null && target.tag == "Villager" || target != null && target.tag == "Chicken"))
        {
            SetState(State_BattleState());
        }

        SetState(State_Idle());

        //SetState (State_Idle()); //Check for possible clashes with the on collision enter thing. Just in case.
    }

    private IEnumerator State_BattleState()
    {
        if (target.tag == "Knight")
        {
            target.GetComponent<Knight>().targetSlime = (this);
        }
        currentCoroutine = "BattleState";
        agent.isStopped = true;
        //waitTimer = attackSpeed;
        //NavAI targetInfo = target.GetComponent<NavAI>();
        while (target != null && target.GetComponent<NavAI>().hp > 0)
        {

            battle = true;
            yield return null;
            //if (waitTimer >= attackSpeed)
            //         {
            //	//Space reserved to trigger ATTACK ANIMATION.
            //	targetInfo.hp -= 1;
            //	waitTimer -= attackSpeed;
            //}
            //waitTimer += Time.deltaTime;
            //yield return null;
        }
		Debug.Log("Ended battle phase");
        battle = false;
        target = null;
        SetState(State_Eating());
    }

    private IEnumerator State_Eating()
    {
        currentCoroutine = "Eating";
        //Space reserved to trigger EAT ANIMATION.
        agent.isStopped = true;

        //Modify to use NPCType variable in NavAI
        if (target == null)
        {
            SetState(State_Idle());
        }
        //target.GetComponent<NavAI>().hp--;
        switch (target.tag)
        {
            case "Villager":
                DuplicateSlime();
                break;
            case "Chicken":
                if (slimeType == 1)
                    ApplyType(2);
				else
					DuplicateSlime();
                break;
            case "Knight":
                if (slimeType == 1)
                    if (target.GetComponent<NavAI>().hp <= 0)
                    {
                        ApplyType(3);
                    }
				else
					DuplicateSlime();
                break;
        }

        yield return new WaitForSecondsRealtime(0.5f);
        //timer wont work
        /*waitTimer = 0f;
        while(waitTimer < eatingSpeed)
        {
            waitTimer += Time.deltaTime;
            yield return null;
        }*/

        //Debug.Log("Slime #" + allSlimes.IndexOf(this) + " is eating Villager #" + allNPC.IndexOf(target.GetComponent<NavAI>()));
        //allNPC.Remove (target.GetComponent<NavAI>());
        //Destroy (target);
        target = null;


        SetState(State_Idle());
    }

    //Follow player state separate from moving to target so the slimes won't change target once they are nearby enough. Might merge.
    private IEnumerator State_FollowPlayer()
    {
        currentCoroutine = "FollowPlayer";
        while (true)
        {
            agent.SetDestination(target.transform.position);
            yield return null;
        }
    }

    private void LookForTarget()
    {
        //set a static list allNPC on the NavAI.
        float shortestDistance = 0f;
        float currentDistanceCheck = 0f;
        if(allNPC.Count == 0)
        {
            GameManager.GameWon();
        }
        foreach (NavAI possibleTarget in allNPC)
        {
            //Iterating through every target might be a little bit heavy. Consider getting a smaller list within a detection range.
            if (possibleTarget != null)
            {
                if(_SlimeSize == SlimeSize.Small)
                {
                    currentDistanceCheck = Vector3.Distance(possibleTarget.transform.position, transform.position);
                }
                else
                {
					currentDistanceCheck = Vector3.Distance(possibleTarget.transform.position, transform.position / transform.localScale.magnitude);
                }
                if ((!target || currentDistanceCheck < shortestDistance) && currentDistanceCheck < detectionRange)
                {
                    target = possibleTarget.gameObject;
                    shortestDistance = currentDistanceCheck;
                }
            }
        }
    }

    private void DuplicateSlime()
    {
        GameObject newSlime = Instantiate(duplicatedSlimePrefab, transform.position, transform.rotation);
        newSlime.GetComponent<UpdatedSlimeAI>().ApplyType(slimeType);
        transform.position = transform.position - new Vector3(-1.5f, 0f, 0f);
    }

    //Hardcoded to always merge to medium slime. To fix when the merging and the rest of the code is actually fixed.
    // private void MergeSlime(GameObject otherSlime)
    // {
    //     //Change the position and rotation to average between the merged slimes when there's time.
    //     GameObject newSlime = Instantiate(_MergeIntoPrefab, transform.position, transform.rotation);

    //     Destroy(otherSlime);
    //     //Destroy(gameObject);
    // }

    public void FollowPlayer(GameObject other)
    {
        if (other.tag == "Player")
        {
			followingPlayer = true;
            target = other;
            SetState(State_FollowPlayer());
        }
    }

    public void StopFollowPlayer()
    {
        //agent.SetDestination (target.transform.position);
		if (followingPlayer){
			followingPlayer = false;
			target = null;
			SetState(State_Idle());  
		}
    }

    private void OnDestroy()
    {
        StopAllCoroutines(); //Check if actually good to use.
        PlayerController_Alpha.followingSlimes.Remove(this.gameObject);
        allSlimes.Remove(this);
        if (allSlimes.Count == 0)
            GameManager.GameLost();
    }

    #region SlimeTypeInitialization
    private void ApplyType()
    {
        switch (slimeType)
        {
            case 1:
                InitNormalSlime();
                break;
            case 2:
                InitChickenSlime();
                break;
            case 3:
                InitArmorSlime();
                break;
        }
    }

    public void ApplyType(int newType)
    {
        slimeType = newType;
        ApplyType();
    }

    private void InitNormalSlime()
    {
        //Assign normal slime mesh.
        agent.speed = normalSlimeSpeed;
        hp = 1;
        IsMerging = false;
    }

    private void InitChickenSlime()
    {
        if(target.GetComponent<NavAI>().hasBeenKilled == false)
        {
            GetComponentInChildren<Renderer>().material = chickenMaterial;
            GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh = chickenMesh;
		    wings.SetActive (true);
		    //Change in slime stats
            agent.speed = chickenSlimeSpeed;
            hp = 1;
            IsMerging = true;
		    slimeType = 2;
        }
    }

    private void InitArmorSlime()
    {
        if(target.GetComponent<NavAI>().hasBeenKilled == false)
        {
            //Assign armors slime mesh and material. Sometimet it creates monsters because slimes with type 2 or 3 are INITIALIZED AGAIN somewhere.
            GetComponentInChildren<Renderer>().material = armorMaterial;
            GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh = armorMesh;
            feather.SetActive(true);
		    //Change in slime stats
            agent.speed = armorSlimeSpeed;
            hp = armorSlimeHp;
            IsMerging = true;
		    slimeType = 3;
        }
    }
    #endregion SlimeTypeInitialization

    public SlimeSize GetSlimeSize()
    {
        return _SlimeSize;
    }
    
    //Ugly, ugly code, but should get merging to work. Later move merging to its own state, and detect nearby slimes.
    private void OnTriggerEnter(Collider other)
    {
        // if(UnityEngine.Random.value < 0.3f && other.gameObject.tag == "Slime")
        //     MergeSlime(other.gameObject);

        if (IsMerging) return;

        var otherSlime = other.gameObject.GetComponent<UpdatedSlimeAI>();
        if (otherSlime != null && otherSlime.IsMerging == false && otherSlime.GetSlimeSize() == _SlimeSize)
        {
            otherSlime.IsMerging = true;
            IsMerging = true;
            var newSlime = Instantiate(_MergeIntoPrefab, transform.position, transform.rotation);
            PlayerController_Alpha.followingSlimes.Add(newSlime);
            newSlime = null;
            Destroy(otherSlime.gameObject);
            Destroy(gameObject);
        }
    }
}

public enum SlimeSize
{
    Small,
    Meduim,
    Big
    
}