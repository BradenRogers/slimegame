﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    private bool slimeBehind;
    [SerializeField] private LayerMask slimeLayer;
    [SerializeField] protected Vector3 slimeTestPosition;
    [SerializeField] private float slimeTestDistance = 1f;
    [Range(0.0f,1.0f)] [SerializeField] private float transparencyAmount;
    private Vector3 origin;
    private Color colour;
    private int raycastTimer = 0;
    [SerializeField] int rayCastEveryxFrames = 10;

    public Vector3 Posotion
    {
        get
        {
            return (Vector3)transform.position;
        }
    }

    private void Start()
    {
        colour = GetComponent<MeshRenderer>().material.color;
        origin = Posotion + slimeTestPosition;
    }

    private void Update()
    {
        raycastTimer++;

        if(raycastTimer >= rayCastEveryxFrames)
        {
            raycastTimer = 0;
            if(Physics.Raycast(origin, Vector3.forward, slimeTestDistance, slimeLayer))
            {
                //Debug.DrawRay(origin, Vector3.forward * slimeTestDistance, Color.red);
                colour.a = transparencyAmount;
                this.GetComponent<MeshRenderer>().material.color = colour;
            }
            else
            {
                //Debug.DrawRay(origin, Vector3.forward * slimeTestDistance, Color.green);
                colour.a = 1.0f;
                this.GetComponent<MeshRenderer>().material.color = colour;
            }
        }
    }


}
