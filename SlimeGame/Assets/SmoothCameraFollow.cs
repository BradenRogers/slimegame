﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCameraFollow : MonoBehaviour 
{
	public Transform target;
	public Vector3 offset;
	private Vector3 velocity = Vector3.zero;
	[SerializeField] public float smoothSpeed = 0.1f;
	[SerializeField] float camRotSpeed = 1f; 

	private void LateUpdate()
	{
		Vector3 desiredPosition = target.position + offset;
		Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothSpeed);
		transform.position = smoothedPosition;

		float scroll = Input.GetAxis ("Mouse ScrollWheel");
		
        transform.Rotate(0,  scroll * camRotSpeed, 0, Space.World);	

		//transform.LookAt(target);
	
	}

}
