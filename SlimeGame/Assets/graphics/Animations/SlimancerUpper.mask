%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: SlimancerUpper
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: SlimancerArmature
    m_Weight: 1
  - m_Path: SlimancerArmature/Root
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Hip_CTRL
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/L_Foot_CTRL
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/R_Foot_CTRL
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/Cloak1
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/Cloak1/Cloak1_001
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/Cloak2
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/Cloak2/Cloak2_001
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/Cloak3
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/Cloak3/Cloak3_001
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/L_LegUpper
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/L_LegUpper/L_LegLower
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/L_LegUpper/L_LegLower/L_Foot
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/R_LegUpper
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/R_LegUpper/R_LegLower
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/R_LegUpper/R_LegLower/R_Foot
    m_Weight: 0
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/L_ArmUpper
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/L_ArmUpper/L_ArmLower
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/L_ArmUpper/L_ArmLower/L_Hand
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/L_ArmUpper/L_ArmLower/L_Hand/Bell
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/L_ArmUpper/L_ArmLower/L_Hand/Bell/BellHammer
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/Neck
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/Neck/Head
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/Neck/Head/HatTop
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/Neck/Head/Nose
    m_Weight: 1
  - m_Path: SlimancerArmature/Root/Spine1_002/Spine1/R_ShoulderTip
    m_Weight: 1
  - m_Path: SlimancerMesh
    m_Weight: 0
