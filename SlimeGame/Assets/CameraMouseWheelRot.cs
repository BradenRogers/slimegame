﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouseWheelRot : MonoBehaviour 
{
	[SerializeField] float camRotSpeed = 1f; 

	private void LateUpdate()
	{
		float scroll = Input.GetAxis ("Mouse ScrollWheel");
		
        transform.Rotate(0,  scroll * camRotSpeed, 0, Space.World);	

		//transform.LookAt(target);
	
	}

}
